const express = require('express')
const conf = require('./conf')

const {build_rpc_runner, build_rpc_router} = require('jrpc-srv')


const app = new express()

const runner = build_rpc_runner(require('./routes/rpc'))
app.use('/rpc', build_rpc_router(runner))

app.use(express.static(__dirname + '/ui/'))

app.listen(conf.port)
console.log(`Listen on port ${conf.port}`)