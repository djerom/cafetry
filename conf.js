require('dotenv').config()

const db = {
    process
}

module.exports = {
    port: process.env.PORT || 3000,

    db: {
        dialect: process.env.DB_TYPE || 'sqlite',
        storage: process.env.DB_FILE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        name: process.env.DB_NAME,
        user: process.env.DB_USER,
        pass: process.env.DB_PASS,
    }
}