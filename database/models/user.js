const { DataTypes } = require("sequelize");

/**
 * 
 * @param {Sequalize} seq 
 */
module.exports = seq => {
    seq.define('User', {
        name: DataTypes.STRING,
        pass: DataTypes.STRING
    })
}