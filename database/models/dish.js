const { DataTypes } = require("sequelize");

/**
 * 
 * @param {Sequalize} seq 
 */
module.exports = seq => {
    seq.define('Dish', {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        photos: [DataTypes.STRING],

        // food_group
        // 
        
        price: DataTypes.INTEGER,
    })
}