const { DataTypes } = require("sequelize");
const crypto = require('crypto')

/**
 * 
 * @param {Sequalize} seq 
 */
module.exports = seq => {
    const model = seq.define('Account', {
        type: DataTypes.STRING, // telegram, viber, email, messanger
        login: DataTypes.STRING,
        password: {
            type: DataTypes.STRING,
            set(value) {
                const hash = model.encrypt(value + this.login)
                
                this.setDataValue('password', hash);
            }
        }
    })

    model.encrypt = s => crypto.createHash('sha256').update(s).digest('hex')
    
    model.fromLogopass = (login, password) => {
        return model.findOne({where: {login, password: model.encrypt((password || "") + login)}})
    }
}