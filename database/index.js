const { readdirSync } = require("fs");
const { Sequelize } = require("sequelize");
const conf = require("../conf");

const seq = new Sequelize({...conf.db, logging: false});

function loadModels(){
    const files = readdirSync(__dirname + '/models')
    files.forEach(x => require(__dirname + `/models/${x}`)(seq))
}

loadModels()
require('./build-relations')(seq)

function connect(){
    
}

module.exports = seq