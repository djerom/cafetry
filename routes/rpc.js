const { use } = require('jrpc-srv/rpc_routes');
const db = require('../database')

module.exports = {
    test() {
        return 'Rpc ready';
    },

    /**
     * 
     * @param {*} login 
     * @param {*} passwrod 
     * @returns 
     */
    login(login, password){
        const { User, Account } = db.models

        return Account.fromLogopass(login, password)
    },

    async users() {
        const { User, Account } = db.models

        const user = await User.create({ name: 'user1' })
        const account = await Account.create({ login: 'Account2', password: 'Password 1' })
        await user.addAccount(account)
        return user

    },

    appendAccount(user_id, type, login, password){

    }
}